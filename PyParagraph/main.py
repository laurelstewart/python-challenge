'''
n this challenge, you get to play the role of chief linguist at a local learning academy. 
As chief linguist, you are responsible for assessing the complexity of various passages of writing, 
ranging from the sophomoric Twilight novel to the nauseatingly high-minded research article. 
Having read so many passages, you've since come up with a fairly simple set of metrics for assessing complexity.

Your task is to create a Python script to automate the analysis of any such passage using these metrics. 

Your script will need to do the following:

Import a text file filled with a paragraph of your choosing.

Assess the passage for each of the following:

Approximate word count
Approximate sentence count
Approximate letter count (per word)
Average sentence length (in words)

As an example, this passage:

“Adam Wayne, the conqueror, with his face flung back and his mane like a lion's, 
stood with his great sword point upwards, the red raiment of his office flapping 
around him like the red wings of an archangel. And the King saw, he knew not how,
 something new and overwhelming. The great green trees and the great red robes 
 swung together in the wind. The preposterous masquerade, born of his own mockery,
  towered over him and embraced the world. This was the normal, this was sanity, 
  this was nature, and he himself, with his rationality, and his detachment and 
  his black frock-coat, he was the exception and the accident a blot of black upon a world of crimson and gold.”

...would yield these results:
Paragraph Analysis
-----------------
Approximate Word Count: 122
Approximate Sentence Count: 5
Average Letter Count: 4.6
Average Sentence Length: 24.0


Special Hint: You may find this code snippet helpful when determining sentence length 
(look into regular expressions if interested in learning more):

import re
re.split("(?<=[.!?]) +", paragraph)
'''
import os
import re
import statistics

myParagraph = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'raw_data', 'paragraph_1.txt')

f=open(myParagraph, "r")
contents = f.read()


# get word count << count spaces plus 1

def word_count(paragraph):
    substring = " "
    my_word_count = paragraph.count(substring) + 1
    return my_word_count


# get sentence count << a sentence ends in a ., ?, or ! looks for trailing space + 1 to not count a string like "1.25" as a sentence. Plus one for the last sentence (that won't have a trailing space)

def sentence_count(paragraph):
    my_sentence_count = len(re.split("(?<=[.!?]) +", paragraph))
    return my_sentence_count

# average word length << first pop all the puncuation, then split on spaces, then get average length

def ave_word_length(paragraph):
    cleaned = paragraph.replace(",", "")
    cleaned = cleaned.replace(".", "")
    cleaned = cleaned.replace("?", "")
    cleaned = cleaned.replace("'", "")
    cleaned = cleaned.split(" ")

    myLengths = []
    for words in cleaned:
        myLengths.append(len(words))

    average = statistics.mean(myLengths)
    
    return average

# sentence length << re.split("(?<=[.!?]) +", paragraph)

def sentence_length(paragraph):
    mySentences = re.split("(?<=[.!?]) +", paragraph)
    wordsPerSentence = []
    for sentences in mySentences:
        wordsPerSentence.append(word_count(sentences))
    word_average = statistics.mean(wordsPerSentence)
    
    return word_average  
    
print("Paragraph Analysis")
print("-----------------")
print("Approximate Word Count: " + str(word_count(contents)))
print("Approximate Sentence Count: " + str(sentence_count(contents)))
print("Average Letter Count: " + str(round(ave_word_length(contents),1)))
print("Approximate Sentence Length: " + str(round(sentence_length(contents),1)))

# now write this to an output file

f = open("paragraph_analysis.txt", "w")

f.write("Paragraph Analysis")
f.write('\n')
f.write("-----------------")
f.write('\n')
f.write("Approximate Word Count: " + str(word_count(contents)))
f.write('\n')
f.write("Approximate Sentence Count: " + str(sentence_count(contents)))
f.write('\n')
f.write("Average Letter Count: " + str(round(ave_word_length(contents),1)))
f.write('\n')
f.write("Approximate Sentence Length: " + str(round(sentence_length(contents),1)))    

