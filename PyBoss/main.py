'''
In this challenge, you get to be the boss. You oversee hundreds of employees across the country developing Tuna 2.0, a world-changing snack food based on canned tuna fish. Alas, being the boss isn't all fun, games, and self-adulation. The company recently decided to purchase a new HR system, and unfortunately for you, the new system requires employee records be stored completely differently.
Your task is to help bridge the gap by creating a Python script able to convert your employee records to the required format. Your script will need to do the following:

Import the employee_data1.csv and employee_data2.csv files, which currently holds employee records like the below:

Emp ID,Name,DOB,SSN,State
214,Sarah Simpson,1985-12-04,282-01-8166,Florida
15,Samantha Lara,1993-09-08,848-80-7526,Colorado
411,Stacy Charles,1957-12-20,658-75-8526,Pennsylvania

Then convert and export the data to use the following format instead:

Emp ID,First Name,Last Name,DOB,SSN,State
214,Sarah,Simpson,12/04/1985,***-**-8166,FL
15,Samantha,Lara,09/08/1993,***-**-7526,CO
411,Stacy,Charles,12/20/1957,***-**-8526,PA

In summary, the required conversions are as follows:

The Name column should be split into separate First Name and Last Name columns.

The DOB data should be re-written into MM/DD/YYYY format.

The SSN data should be re-written such that the first five numbers are hidden from view.

The State data should be re-written as simple two-letter abbreviations.
Special Hint: You may find this link to be helpful—Python Dictionary for State Abbreviations.

'''
# First we'll import the os module
# This will allow us to create file paths across operating systems
import os

# Module for reading CSV files
import csv

# here's a handy dictionary of state abbreviations

us_state_abbrev = {
    'Alabama': 'AL',
    'Alaska': 'AK',
    'Arizona': 'AZ',
    'Arkansas': 'AR',
    'California': 'CA',
    'Colorado': 'CO',
    'Connecticut': 'CT',
    'Delaware': 'DE',
    'Florida': 'FL',
    'Georgia': 'GA',
    'Hawaii': 'HI',
    'Idaho': 'ID',
    'Illinois': 'IL',
    'Indiana': 'IN',
    'Iowa': 'IA',
    'Kansas': 'KS',
    'Kentucky': 'KY',
    'Louisiana': 'LA',
    'Maine': 'ME',
    'Maryland': 'MD',
    'Massachusetts': 'MA',
    'Michigan': 'MI',
    'Minnesota': 'MN',
    'Mississippi': 'MS',
    'Missouri': 'MO',
    'Montana': 'MT',
    'Nebraska': 'NE',
    'Nevada': 'NV',
    'New Hampshire': 'NH',
    'New Jersey': 'NJ',
    'New Mexico': 'NM',
    'New York': 'NY',
    'North Carolina': 'NC',
    'North Dakota': 'ND',
    'Ohio': 'OH',
    'Oklahoma': 'OK',
    'Oregon': 'OR',
    'Pennsylvania': 'PA',
    'Rhode Island': 'RI',
    'South Carolina': 'SC',
    'South Dakota': 'SD',
    'Tennessee': 'TN',
    'Texas': 'TX',
    'Utah': 'UT',
    'Vermont': 'VT',
    'Virginia': 'VA',
    'Washington': 'WA',
    'West Virginia': 'WV',
    'Wisconsin': 'WI',
    'Wyoming': 'WY',
}

# here's my employee data - it's in a folder called resources in that lives at the same level as main.py
employee_data_csv = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'resources', 'employee_data.csv')

# empty dictionary to catch votes should be:
# myEmployees = {
#                   {ID :
#                        "first name":
#                        "last name":
#                         "DOB":
#                         "SSN_hash":
#                         "state":   }
#               }
myEmployees = {}

# function to split 'Name' from data file into first name and last name - split on space
# 
def get_first_name(name):
    first_name = (name.split()[0])
    return first_name
    
def get_last_name(name):
    last_name = (name.split()[1])
    return last_name

def formatDOB(dob):
    mmddyyyyDOB = str(dob[5:7] + "/"  + str(dob[-2:])+ "/" + str(dob[0:4]))
    return mmddyyyyDOB 

# function to hash all but last 4 of SSN

def SSN_hash(ssn):
    hash=str("***-**-") + str(ssn[-4:])
    return hash

def match_state(state):
    abbrState = us_state_abbrev.get(state)
    return abbrState

with open(employee_data_csv, newline='') as csvfile:

    # CSV reader specifies delimiter and variable that holds contents
    csvreader = csv.reader(csvfile, delimiter=',')

    # Read the header row first
    csv_header = next(csvreader)
    # print(f"CSV Header: {csv_header}")

    for row in csvreader:
        myEmployees.update({
            row[0]:
            {"first_name": get_first_name(row[1]),
             "last_name": get_last_name(row[1]),
             "DOB": formatDOB(row[2]),
             "SSN_hash": SSN_hash(row[3]),
             "state": match_state(row[4])
            }
        })
# print(myEmployees) << if you want to see the dictionary in the termimal

# now write this to an output file

f = open("employee_data_formatted1.csv", "w")

f.write("Emp ID,First Name,Last Name,DOB,SSN,State")
f.write('\n')
for id_num in myEmployees:
    
    f.write(id_num + "," )
    
    
    KEYS = myEmployees[id_num]

    for values in KEYS:
        f.write(str(KEYS[values]) + ",")
    
    f.write("\n")

f.close()    

with open('employee_data_formatted1.csv', 'r') as r, open('employee_data.csv', 'w') as w:    
    for num, line in enumerate(r):
        if num > 0:            
            newline = line[:-2] + "\n" if "\n" in line else line[:-1]
        else:
            newline = line               
        w.write(newline)

f.close()

os.remove('employee_data_formatted1.csv')
   